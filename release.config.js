const branch = process.env.CI_COMMIT_BRANCH

const config = {
    tagFormat: "${version}",
    branches: [
        "main",
        {
            name: "dev",
            channel: "default",
            prerelease: "beta"
        },
        {
            name: "*",
            channel: "default",
            prerelease: "alpha"
        }
    ],
    plugins: [
        ["@semantic-release/commit-analyzer", {
            preset: "conventionalcommits"
        }],
        ["@semantic-release/release-notes-generator", {
            preset: "conventionalcommits"
        }],
    ]
}

if (config.branches.some(it => it === branch || (it.name === branch && !it.prerelease))) {
    
    config.plugins.push([ "@semantic-release/changelog", {
        changelogTitle: "# Changelog\n\nAll notable changes to this project will be documented in this file. See\n[Conventional Commits](https://conventionalcommits.org) for commit guidelines."
    }]);
    
    config.plugins.push([ "@semantic-release/git", {
        message: "chore(release): ${nextRelease.version}\n\n${nextRelease.notes}"
    }]);
}

module.exports = config