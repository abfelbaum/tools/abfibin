using AbfiBin.Client;
using AbfiBin.Client.Interfaces;
using AbfiBin.Client.Models;
using AbfiBin.Client.Services;
using AbfiBin.Shared;
using AbfiBin.Shared.Validators;
using Blazored.LocalStorage;
using Blazored.SessionStorage;
using FluentValidation;
using ITfoxtec.Identity.BlazorWebAssembly.OpenidConnect;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using MudBlazor.Services;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

// To read a custom configuration file other than appsettings.json
// use HttpClient
using var http = new HttpClient
{
    BaseAddress = new Uri(builder.HostEnvironment.BaseAddress)
};

using HttpResponseMessage response = await http.GetAsync("api/v1/Configuration");
await using Stream stream = await response.Content.ReadAsStreamAsync();
builder.Configuration.AddJsonStream(stream);

builder.Services.Configure<ClientOptions>(builder.Configuration);

builder.Services.AddHttpClient(HttpClients.WithToken,
        client => client.BaseAddress = new Uri(builder.HostEnvironment.BaseAddress))
    .AddHttpMessageHandler(serviceProvider =>
    {
        AccessTokenMessageHandler handler = serviceProvider.GetRequiredService<AccessTokenMessageHandler>();
        handler.AuthorizedUris = new[] { builder.HostEnvironment.BaseAddress };
        return handler;
    });

builder.Services.AddHttpClient(HttpClients.Default,
    client => client.BaseAddress = new Uri(builder.HostEnvironment.BaseAddress));

builder.Services.AddOpenidConnectPkce(options =>
{
    options.Authority = builder.Configuration["Authentication:Authority"];
    options.ClientId = builder.Configuration["Authentication:ClientId"];
    options.ResponseMode = builder.Configuration["Authentication:ResponseMode"];
    options.Scope = builder.Configuration["Authentication:Scope"];
});

builder.Services.AddMudServices();
builder.Services.AddBlazoredLocalStorage();
builder.Services.AddValidatorsFromAssemblyContaining<BinValidator>();

builder.Services.AddTransient<IEditorService, EditorService>();
builder.Services.AddTransient<IUserSettingsService, UserSettingsService>();
builder.Services.AddScoped<LayoutState>();
builder.Services.AddScoped<BinState>();
builder.Services.AddScoped<ISessionStorageService, LocalStorageAsSessionStorageService>();

await builder.Build().RunAsync();