namespace AbfiBin.Client.Models;

public static class HttpClients
{
    public static string WithToken { get; } = "AbfiBin.Server.Token";
    public static string Default { get; } = "AbfiBin.Server";
}