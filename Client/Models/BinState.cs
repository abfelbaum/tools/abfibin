using AbfiBin.Shared;
using Microsoft.Extensions.Options;

namespace AbfiBin.Client.Models;

public class BinState
{

    public BinState(IOptions<ClientOptions> options)
    { 
        Highlighter = options.Value.Defaults.Highlighter;
    }

    public enum ActionType
    {
        Save,
        SaveAs,
        Edit,
        Copy
    }

    public bool IsBin { get; private set; }
    public bool IsLoading { get; private set; }
    public bool IsReadOnly { get; private set; }
    public string? Highlighter { get; private set; }

    public event EventHandler? MajorUpdateOccured;
    public event EventHandler<ActionType>? ActionUpdateOccured;
    public event EventHandler<string?>? HighlighterUpdateOccured;

    private void OnUpdateOccured()
    {
        MajorUpdateOccured?.Invoke(this, EventArgs.Empty);
    }

    private void OnActionUpdateOccured(ActionType type)
    {
        ActionUpdateOccured?.Invoke(this, type);
    }

    private void OnHighlighterUpdateOccured(string? highlighter)
    {
        HighlighterUpdateOccured?.Invoke(this, highlighter);
    }

    public void Save()
    {
        OnActionUpdateOccured(ActionType.Save);
    }

    public void SaveAs()
    {
        OnActionUpdateOccured(ActionType.SaveAs);
    }

    public void Edit()
    {
        OnActionUpdateOccured(ActionType.Edit);
    }

    public void Copy()
    {
        OnActionUpdateOccured(ActionType.Copy);
    }

    public void SetIsBin(bool value)
    {
        if (IsBin == value)
        {
            return;
        }

        IsBin = value;
        OnUpdateOccured();
    }

    public void SetIsLoading(bool value)
    {
        if (IsLoading == value)
        {
            return;
        }

        IsLoading = value;
        OnUpdateOccured();
    }

    public void SetIsReadOnly(bool value)
    {
        if (IsReadOnly == value)
        {
            return;
        }

        IsReadOnly = value;
        OnUpdateOccured();
    }

    public void SetHighlighter(string? value)
    {
        if (Highlighter == value)
        {
            return;
        }

        Highlighter = value;
        OnHighlighterUpdateOccured(value);
    }
}