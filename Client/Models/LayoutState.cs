// Copyright (c) MudBlazor 2021
// MudBlazor licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using AbfiBin.Client.Interfaces;

namespace AbfiBin.Client.Models;

public class LayoutState
{
    public LayoutState(IUserSettingsService userSettingsService)
    {
        IsDarkMode = userSettingsService.GetTheme().IsDark;
    }

    public bool IsDarkMode { get; private set; }
    public event EventHandler MajorUpdateOccured = null!;

    private void OnMajorUpdateOccured()
    {
        MajorUpdateOccured.Invoke(this, EventArgs.Empty);
    }

    public void SetDarkMode(bool value)
    {
        if (IsDarkMode == value)
        {
            return;
        }

        IsDarkMode = value;
        OnMajorUpdateOccured();
    }
}