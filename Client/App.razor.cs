using ITfoxtec.Identity.BlazorWebAssembly.OpenidConnect;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;

namespace AbfiBin.Client;

public partial class App
{
    private bool LoggingIn { get; set; }
    [Inject] private OpenidConnectPkce OpenidConnectPkce { get; set; } = null!;

    [Inject] private IJSRuntime JsRuntime { get; set; } = null!;

    private async Task LoginUser()
    {
        LoggingIn = true;
        await OpenidConnectPkce.LoginAsync();
        await Task.Delay(TimeSpan.FromSeconds(5));
        LoggingIn = false;
    }

    private Task GoBack()
    {
        return JsRuntime.InvokeVoidAsync("history.back").AsTask();
    }
}