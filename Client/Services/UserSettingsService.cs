using AbfiBin.Client.Interfaces;
using AbfiBin.Shared;
using Blazored.LocalStorage;
using Microsoft.Extensions.Options;

namespace AbfiBin.Client.Services;

public class UserSettingsService : IUserSettingsService
{
    private const string ThemeKey = "theme";
    private const string DarkModeKey = "darkMode";
    private const string KeybindingsKey = "keybindings";

    private readonly ISyncLocalStorageService _localStorage;
    private readonly ClientOptions _options;

    public UserSettingsService(ISyncLocalStorageService localStorage, IOptions<ClientOptions> options)
    {
        _localStorage = localStorage;
        _options = options.Value;
    }

    public Theme GetTheme()
    {
        string? themeName = GetOrNull<string>(ThemeKey);


        Theme? theme = _options.Themes.FirstOrDefault(x => x.Name == themeName);
        if (theme is null)
        {
            return _options.Themes.First(x => x.Name == _options.Defaults.Theme);
        }

        return theme;
    }

    public void SetTheme(Theme theme)
    {
        Set(ThemeKey, theme.Name);
        Set(DarkModeKey, theme.IsDark);
    }

    public string GetKeybindings()
    {
        string? keybindings = GetOrNull<string>(KeybindingsKey);

        if (keybindings is null)
        {
            return _options.Defaults.Keybindings;
        }

        return keybindings;
    }

    public void SetKeybindings(string keybindings)
    {
        Set(KeybindingsKey, keybindings);
    }

    private T? GetOrNull<T>(string key)
    {
        return !_localStorage.ContainKey(key) ? default : _localStorage.GetItem<T>(key);
    }

    private void Set<T>(string key, T? value)
    {
        if (value is null)
        {
            if (_localStorage.ContainKey(key))
            {
                _localStorage.RemoveItem(key);
            }

            return;
        }

        _localStorage.SetItem(key, value);
    }
}