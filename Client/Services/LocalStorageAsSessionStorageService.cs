using Blazored.LocalStorage;
using Blazored.SessionStorage;
using ChangedEventArgs = Blazored.SessionStorage.ChangedEventArgs;
using ChangingEventArgs = Blazored.SessionStorage.ChangingEventArgs;

namespace AbfiBin.Client.Services;

public class LocalStorageAsSessionStorageService : ISessionStorageService
{
    private readonly ILocalStorageService _localStorage;

    public LocalStorageAsSessionStorageService(ILocalStorageService localStorage)
    {
        _localStorage = localStorage;
    }

    public ValueTask ClearAsync(CancellationToken? cancellationToken = null)
    {
        return _localStorage.ClearAsync(cancellationToken);
    }

    public ValueTask<T> GetItemAsync<T>(string key, CancellationToken? cancellationToken = null)
    {
        return _localStorage.GetItemAsync<T>(key, cancellationToken);
    }

    public ValueTask<string> GetItemAsStringAsync(string key, CancellationToken? cancellationToken = null)
    {
        return _localStorage.GetItemAsStringAsync(key, cancellationToken);
    }

    public ValueTask<string> KeyAsync(int index, CancellationToken? cancellationToken = null)
    {
        return _localStorage.KeyAsync(index, cancellationToken);
    }

    public ValueTask<bool> ContainKeyAsync(string key, CancellationToken? cancellationToken = null)
    {
        return _localStorage.ContainKeyAsync(key, cancellationToken);
    }

    public ValueTask<int> LengthAsync(CancellationToken? cancellationToken = null)
    {
        return _localStorage.LengthAsync(cancellationToken);
    }

    public ValueTask RemoveItemAsync(string key, CancellationToken? cancellationToken = null)
    {
        return _localStorage.RemoveItemAsync(key, cancellationToken);
    }

    public ValueTask SetItemAsync<T>(string key, T data, CancellationToken? cancellationToken = null)
    {
        return _localStorage.SetItemAsync(key, data, cancellationToken);
    }

    public ValueTask SetItemAsStringAsync(string key, string data, CancellationToken? cancellationToken = null)
    {
        return _localStorage.SetItemAsStringAsync(key, data, cancellationToken);
    }

#pragma warning disable CS0067
    public event EventHandler<ChangingEventArgs>? Changing;

    public event EventHandler<ChangedEventArgs>? Changed;
#pragma warning restore CS0067
}