using AbfiBin.Client.Interfaces;
using AbfiBin.Shared;
using Microsoft.Extensions.Options;
using Microsoft.JSInterop;

namespace AbfiBin.Client.Services;

public class EditorService : IEditorService
{
    private readonly IJSRuntime _jsRuntime;
    private readonly ClientOptions _options;

    public EditorService(IJSRuntime jsRuntime, IOptions<ClientOptions> options)
    {
        _jsRuntime = jsRuntime;
        _options = options.Value;
    }

    public Task SetHighlighter(Guid id, string? highlighter)
    {
        highlighter ??= _options.Defaults.Highlighter;

        if (highlighter == "auto")
        {
            highlighter = "text";
        }

        return _jsRuntime.InvokeVoidAsync("Editor.setMode", id.ToString(), highlighter).AsTask();
    }

    public Task SetTheme(Theme theme)
    {
        return _jsRuntime.InvokeVoidAsync("Editor.setTheme", theme.Name).AsTask();
    }

    public Task Start(Guid id, string? value = null, Theme? theme = null, string highlighter = "auto",
        string? keybindings = null, bool readOnly = false)
    {
        value ??= string.Empty;
        theme ??= _options.Themes.First(x => x.Name == _options.Defaults.Theme);
        keybindings ??= _options.Keybindings.First(x => x == _options.Defaults.Keybindings);


        if (highlighter == "auto")
        {
            highlighter = "text";
        }

        return _jsRuntime.InvokeVoidAsync("Editor.start", id.ToString(), value, theme.Name, highlighter, keybindings,
                readOnly)
            .AsTask();
    }

    public Task Destroy(Guid id)
    {
        return _jsRuntime.InvokeVoidAsync("Editor.destroy", id.ToString()).AsTask();
    }

    public Task<string?> GetHighlighter(Guid id)
    {
        return _jsRuntime.InvokeAsync<string?>("Editor.getLanguage", id.ToString()).AsTask();
    }

    public Task<string> GetContent(Guid id)
    {
        return _jsRuntime.InvokeAsync<string>("Editor.getContent", id.ToString()).AsTask();
    }

    public Task SetKeybindings(string keybindings)
    {
        return _jsRuntime.InvokeVoidAsync("Editor.setKeybindings", keybindings).AsTask();
    }
}