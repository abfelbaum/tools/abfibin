@inject IEditorService EditorService
@inject IOptions<ClientOptions> AppOptions
@inject IUserSettingsService UserSettings
@inject NavigationManager Navigation
@inject IHttpClientFactory HttpFactory
@inject IValidator<Bin> BinValidator
@inject ISnackbar Snackbar
@inject IJSRuntime Js
@inject BinState BinState
@using FluentValidation
@using FluentValidation.Results
@using System.Text
@using Severity = MudBlazor.Severity
@implements IDisposable

<MudElement HtmlTag="div" Class="bin-tabs">
    @if (ReadOnly)
    {
        <MudTabs
            @bind-ActivePanelIndex="_activePanelIndex"
            ApplyEffectsToContainer="true">

            @foreach ((BinFile file, int i) in Bin.Files.Select((x, i) => (x, i)))
            {
                <EditableTabPanel @bind-Text="@file.Name" Tag="@file.Id" ReadOnly="true" Placeholder="@GetName(i)" Selected="_activePanelIndex == i">
                    <FileEditor File="file" ReadOnly="ReadOnly"/>
                </EditableTabPanel>
            }

        </MudTabs>
    }
    else
    {
        <MudDynamicTabs
            @bind-ActivePanelIndex="_activePanelIndex"
            ApplyEffectsToContainer="true"
            AddTab="@AddTabCallback">
            <Header>
                <MudIconButton Disabled="@(Bin.Files.Count >= 10)" Icon="@Icons.Filled.Add" OnClick="@AddTabCallback"/>
            </Header>
            <TabPanelHeader>
                <MudIconButton Disabled="@(Bin.Files.Count <= 1)" Icon="@Icons.Material.Filled.Close" OnClick="() => CloseTabCallback(context)"/>
            </TabPanelHeader>
            <ChildContent>
                @foreach ((BinFile file, int i) in Bin.Files.Select((x, i) => (x, i)))
                {
                    <EditableTabPanel @bind-Text="@file.Name" Tag="@file.Id" Placeholder="@GetName(i)" Selected="_activePanelIndex == i">
                        <FileEditor File="file" ReadOnly="ReadOnly"/>
                    </EditableTabPanel>
                }
            </ChildContent>
        </MudDynamicTabs>
        
        <MudDialog @bind-IsVisible="_isEditingName" Options="_dialogOptions">
            <TitleContent>
                <MudText Typo="Typo.h6">
                    <MudIcon Icon="@Icons.Material.Filled.Edit" Class="mr-3"/> Edit bin name
                </MudText>
            </TitleContent>
            <DialogContent>
                <MudTextField @bind-Value="Bin.Name" Placeholder="Unnamed" AutoFocus="true"/>
            </DialogContent>
            <DialogActions>
                <MudButton Disabled="BinState.IsLoading" OnClick="Save" Variant="Variant.Filled" Color="Color.Primary">
                    @if (BinState.IsLoading)
                    {
                        <MudProgressCircular Class="ms-n1" Size="Size.Small" Indeterminate="true"/>
                    }
                    else
                    {
                        <MudIcon Class="ms-n1" Icon="@Icons.Filled.Save"/>
                    }
                    <MudText Class="ms-2">Save</MudText>
                </MudButton>
            </DialogActions>
        </MudDialog>
    }
</MudElement>

@code
{
    [Parameter]
    public bool ReadOnly { get; set; }


    [Parameter]
    public Bin Bin { get; set; } = null!;

    private int _activePanelIndex;

    private static string GetName(int i)
    {
        return $"File ({i + 1})";
    }

    private BinFile GetActiveFile()
    {
        return _activePanelIndex >= Bin.Files.Count ? Bin.Files.Last() : Bin.Files[_activePanelIndex];
    }

    private HttpClient _http = null!;
    private bool _isEditingName;

    private readonly DialogOptions _dialogOptions = new()
    {
        Position = DialogPosition.Center,
        FullScreen = false,
        CloseButton = true,
        NoHeader = false,
        CloseOnEscapeKey = true,
        DisableBackdropClick = false,
        FullWidth = false
    };

    private void AddTabCallback()
    {
        var file = new BinFile
        {
            Highlighter = AppOptions.Value.Defaults.Highlighter
        };

        Bin.Files.Add(file);
    }

    private void CloseTabCallback(MudTabPanel panel)
    {
        BinFile? file = Bin.Files.FirstOrDefault(x => x.Id == (Guid)panel.Tag);

        if (file is null)
        {
            return;
        }

        EditorService.Destroy(file.Id);
        Bin.Files.Remove(file);
    }

    protected override void OnInitialized()
    {
        BinState.MajorUpdateOccured += OnMajorUpdateOccured;
        BinState.ActionUpdateOccured += OnActionUpdateOccured;
        BinState.HighlighterUpdateOccured += OnHighlighterChanged;
        BinState.SetIsReadOnly(ReadOnly);
        BinState.SetIsBin(true);
        _http = HttpFactory.CreateClient(HttpClients.WithToken);

    // ReSharper disable once ConditionIsAlwaysTrueOrFalseAccordingToNullableAPIContract
        if (Bin is null)
        {
            InitEmptyBin();
        }
    }

    private void InitEmptyBin()
    {
        Bin = new Bin
        {
            Files = new List<BinFile>()
        };

        var file = new BinFile
        {
            Highlighter = AppOptions.Value.Defaults.Highlighter
        };

        Bin.Files.Add(file);
    }

    private async Task Save()
    {
        BinState.SetIsLoading(true);
        await PrepareFiles();

        if (!await Validate())
        {
            BinState.SetIsLoading(false);
            return;
        }

        HttpResponseMessage response = await _http.PostAsJsonAsync("api/v1/Bin", Bin);

        if (!response.IsSuccessStatusCode || response.Headers.Location is null)
        {
            Snackbar.Add(response.ReasonPhrase);
            BinState.SetIsLoading(false);
            return;
        }


        Navigation.NavigateTo($"bin/{await response.Content.ReadFromJsonAsync<Guid>()}");
        BinState.SetIsLoading(false);
    }

    private async Task PrepareFiles()
    {
        foreach (BinFile file in Bin.Files)
        {
            if (file.Highlighter is null or "auto")
            {
                file.Highlighter = await EditorService.GetHighlighter(file.Id) ?? "text";

                if (!AppOptions.Value.Highlighters.Contains(file.Highlighter))
                {
                    KeyValuePair<string, IEnumerable<string>>? mappedHighlighter = AppOptions.Value.Mappings.Highlighters.FirstOrDefault(x => x.Value.Contains(file.Highlighter));

                    if (!mappedHighlighter.HasValue)
                    {
                        file.Highlighter = "text";
                    }

                    file.Highlighter = mappedHighlighter.Value.Key;
                }
            }

            file.Content = await EditorService.GetContent(file.Id);
        }
    }

    private async Task<bool> Validate()
    {
        ValidationResult validationResult = await BinValidator.ValidateAsync(Bin);

        if (!validationResult.IsValid)
        {
            var builder = new StringBuilder();

            builder.Append("<ul>");

            foreach (ValidationFailure? error in validationResult.Errors)
            {
                builder.Append($@"<li>{error.ErrorMessage}</li>");
            }

            builder.Append("</ul>");

            Snackbar.Add(builder.ToString(), Severity.Error);
        }

        return validationResult.IsValid;
    }

    private void OnHighlighterChanged(object? sender, string? highlighter)
    {
        BinFile file = GetActiveFile();

        file.Highlighter = highlighter;
        EditorService.SetHighlighter(file.Id, file.Highlighter);
    }

    private void Edit()
    {
        Navigation.NavigateTo($"/?from={Bin.Id:D}");
    }

    private async Task Copy()
    {
        BinFile file = GetActiveFile();

        await Js.InvokeVoidAsync("navigator.clipboard.writeText", file.Content);
        Snackbar.Add("Copied file to clipboard!", Severity.Success);
    }

    public void Dispose()
    {
        BinState.SetIsBin(false);
        BinState.MajorUpdateOccured -= OnMajorUpdateOccured;
        BinState.ActionUpdateOccured -= OnActionUpdateOccured;
        BinState.HighlighterUpdateOccured -= OnHighlighterChanged;
    }

    private void OnMajorUpdateOccured(object? sender, EventArgs e)
    {
        StateHasChanged();
    }

    private void OnActionUpdateOccured(object? sender, BinState.ActionType action)
    {
        switch (action)
        {
            case BinState.ActionType.Save:
#pragma warning disable CS4014
                Save();
#pragma warning restore CS4014
                break;
            case BinState.ActionType.SaveAs:
                _isEditingName = true;
                StateHasChanged();
                break;
            case BinState.ActionType.Edit:
                Edit();
                break;
            case BinState.ActionType.Copy:
#pragma warning disable CS4014
                Copy();
#pragma warning restore CS4014
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(action), action, null);
        }
    }
}