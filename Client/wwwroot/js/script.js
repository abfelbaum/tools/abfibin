const Editor = {
    instances: {},
    start: function (id, value, theme, mode, keybindings, readonly) {
        this.instances[id] = ace.edit(id, {
            mode: "ace/mode/" + mode,
            theme: "ace/theme/" + theme,
            readOnly: readonly,
            value: value,
            keyboardHandler: "ace/keyboard/" + keybindings
        });
    },
    getInstance: function (id) {
        return this.instances[id];
    },
    destroy: function (id) {
        const instance = this.getInstance(id);

        instance.destroy();

        delete this.instances[id];
    },
    setMode: function (id, language) {
        const instance = this.getInstance(id);

        instance.session.setMode("ace/mode/" + language);
    },
    setTheme: function (theme) {
        for (const id in this.instances) {
            const instance = this.getInstance(id);
            instance.setTheme("ace/theme/" + theme);
        }
    },
    setKeybindings: function (keybindings) {
        for (const id in this.instances) {
            const instance = this.getInstance(id)
            instance.setKeyboardHandler("ace/keyboard/" + keybindings);
        }
    },
    getLanguage: function (id) {
        const instance = this.getInstance(id);

        const result = hljs.highlightAuto(instance.session.getValue());

        if (!result.language) {
            return null;
        }

        return result.language;
    },
    getContent: function (id) {
        const instance = this.getInstance(id);

        return instance.session.getValue();
    }
}

window.Editor = Editor;