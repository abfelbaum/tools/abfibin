using AbfiBin.Shared;

namespace AbfiBin.Client.Interfaces;

public interface IEditorService
{
    public Task SetHighlighter(Guid id, string? highlighter);
    public Task SetTheme(Theme theme);
    public Task SetKeybindings(string keybindings);

    public Task Start(Guid id, string? value = null, Theme? theme = null, string highlighter = "auto",
        string? keybindings = null, bool readOnly = false);

    public Task Destroy(Guid id);
    public Task<string?> GetHighlighter(Guid id);
    public Task<string> GetContent(Guid id);
}