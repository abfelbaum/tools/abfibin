using AbfiBin.Shared;

namespace AbfiBin.Client.Interfaces;

public interface IUserSettingsService
{
    public Theme GetTheme();
    public void SetTheme(Theme theme);
    public string GetKeybindings();
    public void SetKeybindings(string keybindings);
}