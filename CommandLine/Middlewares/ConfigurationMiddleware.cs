using System.CommandLine.Hosting;
using System.CommandLine.Invocation;
using AbfiBin.CommandLine.Commands;
using AbfiBin.CommandLine.Models;
using Microsoft.Extensions.DependencyInjection;

namespace AbfiBin.CommandLine.Middlewares;

public class ConfigurationMiddleware
{
    public static Task GlobalConfigurationMiddleware(InvocationContext context, Func<InvocationContext, Task> next)
    {
        AppConfiguration appConfiguration = context.GetHost().Services.GetRequiredService<AppConfiguration>();

        appConfiguration.Address = context.ParseResult.GetValueForOption(GlobalOptions.Address)
                                   ?? throw new InvalidOperationException();
        appConfiguration.Authority = context.ParseResult.GetValueForOption(GlobalOptions.Authority)
                                     ?? throw new InvalidOperationException();
        appConfiguration.Scope = context.ParseResult.GetValueForOption(GlobalOptions.Scope)
                                 ?? throw new InvalidOperationException();
        appConfiguration.CacheDirectory = context.ParseResult.GetValueForOption(GlobalOptions.CacheDirectory)
                                          ?? throw new InvalidOperationException();
        appConfiguration.ClientId = context.ParseResult.GetValueForOption(GlobalOptions.ClientId)
                                    ?? throw new InvalidOperationException();
        appConfiguration.RedirectPort = context.ParseResult.GetValueForOption(GlobalOptions.RedirectPort);
        appConfiguration.RedirectUri = context.ParseResult.GetValueForOption(GlobalOptions.RedirectUri)
                                       ?? throw new InvalidOperationException();
        appConfiguration.CacheFileName = context.ParseResult.GetValueForOption(GlobalOptions.CacheFileName)
                                         ?? throw new InvalidOperationException();

        appConfiguration.AccessToken = context.ParseResult.GetValueForOption(GlobalOptions.Token);

        return next(context);
    }
}