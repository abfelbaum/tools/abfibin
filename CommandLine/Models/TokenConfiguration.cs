namespace AbfiBin.CommandLine.Models;

public class TokenConfiguration
{
    public string AccessToken { get; set; } = null!;
    public DateTimeOffset ExpiresAt { get; set; }
    public string RefreshToken { get; set; } = null!;
}