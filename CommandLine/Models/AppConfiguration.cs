using System.Text.Json.Serialization;

namespace AbfiBin.CommandLine.Models;

public class AppConfiguration
{
    public Uri Address { get; set; } = null!;
    public Uri Authority { get; set; } = null!;
    public string ClientId { get; set; } = null!;
    public Uri RedirectUri { get; set; } = null!;
    public int RedirectPort { get; set; }
    public string Scope { get; set; } = null!;
    public string CacheDirectory { get; set; } = null!;
    public string CacheFileName { get; set; } = null!;
    public string? AccessToken { get; set; }
}