using AbfiBin.CommandLine.Interfaces;
using AbfiBin.CommandLine.Models;
using IdentityModel.OidcClient;
using IdentityModel.OidcClient.Results;

namespace AbfiBin.CommandLine.Services;

public class OidcClientService : IOidcClientService
{
    private readonly OidcClient _oidcClient;

    public OidcClientService(AppConfiguration configuration)
    {
        var clientOptions = new OidcClientOptions
        {
            Authority = configuration.Authority.ToString(),
            ClientId = configuration.ClientId,
            RedirectUri = configuration.RedirectUri.ToString(),
            Scope = configuration.Scope,
            FilterClaims = false,
            Browser = new SystemBrowser(configuration.RedirectPort),
            IdentityTokenValidator = new JwtHandlerIdentityTokenValidator(),
            RefreshTokenInnerHttpHandler = new SocketsHttpHandler()
        };
        
        _oidcClient = new OidcClient(clientOptions);
    }

    public async Task<TokenConfiguration> Login(CancellationToken cancellationToken = default)
    {
        LoginResult result = await _oidcClient.LoginAsync(new LoginRequest(), cancellationToken);

        if (result.IsError)
        {
            throw new Exception($"Login failed: {result.ErrorDescription}");
        }
        
        return new TokenConfiguration
        {
            AccessToken = result.AccessToken,
            ExpiresAt = result.AccessTokenExpiration,
            RefreshToken = result.RefreshToken
        };
    }

    public async Task<TokenConfiguration> Refresh(TokenConfiguration tokenConfiguration, CancellationToken cancellationToken = default)
    {
        RefreshTokenResult result = await _oidcClient.RefreshTokenAsync(tokenConfiguration.RefreshToken, cancellationToken: cancellationToken);

        if (result.IsError)
        {
            throw new Exception($"Token refresh failed: {result.ErrorDescription}");
        }

        return new TokenConfiguration
        {
            AccessToken = result.AccessToken,
            ExpiresAt = result.AccessTokenExpiration,
            RefreshToken = result.RefreshToken
        };
    }

    public async Task<bool> IsValid(TokenConfiguration tokenConfiguration, CancellationToken cancellationToken = default)
    {
        if (tokenConfiguration.ExpiresAt.ToUniversalTime() < DateTimeOffset.UtcNow)
        {
            return false;
        }
        
        UserInfoResult result = await _oidcClient.GetUserInfoAsync(tokenConfiguration.AccessToken, cancellationToken);

        return !result.IsError;
    }
}