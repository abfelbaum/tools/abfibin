using AbfiBin.CommandLine.Interfaces;
using AbfiBin.CommandLine.Models;
using Microsoft.Identity.Client.Extensions.Msal;

namespace AbfiBin.CommandLine.Services;

public class SecureStorageService : ISecureStorageService
{
    private readonly Storage _storage;

    public SecureStorageService(AppConfiguration configuration)
    {
        _cacheDirectory = configuration.CacheDirectory;
        _cacheFileName = configuration.CacheFileName;
        _storage = CreateStorage();
    }

    public void Verify()
    {
        _storage.VerifyPersistence();
    }

    public void Write(byte[] data)
    {
        _storage.WriteData(data);
    }

    public byte[] Read()
    {
        return _storage.ReadData();
    }

    public void Clear()
    {
        _storage.Clear();
    }

    private Storage CreateStorage()
    {
        StorageCreationProperties storageProperties;
        try
        {
            storageProperties = ConfigureSecureStorage(usePlaintextFileOnLinux: false);

            return Storage.Create(storageProperties);
        }
        catch (MsalCachePersistenceException ex)
        {
            Console.WriteLine("Cannot persist data securely. ");
            Console.WriteLine("Details: " + ex);


            if (!SharedUtilities.IsLinuxPlatform())
            {
                throw;
            }

            storageProperties = ConfigureSecureStorage(usePlaintextFileOnLinux: true);

            Console.WriteLine($"Falling back on using a plaintext " +
                              $"file located at {storageProperties.CacheFilePath} Users are responsible for securing this file!");


            return Storage.Create(storageProperties);
        }
    }

    private readonly string _cacheFileName;
    private readonly string _cacheDirectory;
    private const string LinuxKeyRingSchema = "dev.abfelbaum.abfibin.tokencache";
    private const string LinuxKeyRingCollection = MsalCacheHelper.LinuxKeyRingDefaultCollection;
    private const string LinuxKeyRingLabel = "Token cache for AbfiBin CLI";

    private static readonly KeyValuePair<string, string> LinuxKeyRingAttr1 = new("Version", "1");
    private static readonly KeyValuePair<string, string> LinuxKeyRingAttr2 = new("ProductGroup", "MyApps");

    private const string KeyChainServiceName = "abfibin_cli_service";
    private const string KeyChainAccountName = "abfibin_cli_account";


    private StorageCreationProperties ConfigureSecureStorage(bool usePlaintextFileOnLinux)
    {
        if (!usePlaintextFileOnLinux)
        {
            return new StorageCreationPropertiesBuilder(
                    _cacheFileName,
                    _cacheDirectory)
                .WithLinuxKeyring(
                    LinuxKeyRingSchema,
                    LinuxKeyRingCollection,
                    LinuxKeyRingLabel,
                    LinuxKeyRingAttr1,
                    LinuxKeyRingAttr2)
                .WithMacKeyChain(
                    KeyChainServiceName,
                    KeyChainAccountName)
                .Build();
        }

        return new StorageCreationPropertiesBuilder(
                $"{_cacheFileName}.plaintext", // do not use the same file name so as not to overwrite the encypted version
                _cacheDirectory)
            .WithLinuxUnprotectedFile()
            .WithMacKeyChain(
                KeyChainServiceName,
                KeyChainAccountName)
            .Build();
    }
}