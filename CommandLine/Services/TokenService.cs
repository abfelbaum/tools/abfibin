using AbfiBin.CommandLine.Interfaces;
using AbfiBin.CommandLine.Models;

namespace AbfiBin.CommandLine.Services;

public class TokenService : ITokenService
{
    private readonly AppConfiguration _configuration;
    private readonly ITokenConfigurationService _tokenConfigurationService;
    private readonly IOidcClientService _oidcClientService;

    public TokenService(AppConfiguration configuration, ITokenConfigurationService tokenConfigurationService, IOidcClientService oidcClientService)
    {
        _configuration = configuration;
        _tokenConfigurationService = tokenConfigurationService;
        _oidcClientService = oidcClientService;
    }

    public async Task<string> Get(CancellationToken cancellationToken = default)
    {
        if (_configuration.AccessToken is not null)
        {
            return _configuration.AccessToken;
        }
        
        TokenConfiguration? tokenConfiguration = _tokenConfigurationService.Get();
        if (tokenConfiguration is null)
        {
            tokenConfiguration = await _oidcClientService.Login(cancellationToken);
            _tokenConfigurationService.Set(tokenConfiguration);
        }

        if (await _oidcClientService.IsValid(tokenConfiguration, cancellationToken))
        {
            return tokenConfiguration.AccessToken;
        }

        try
        {
            tokenConfiguration = await _oidcClientService.Refresh(tokenConfiguration, cancellationToken);
        }
        catch
        {
            _tokenConfigurationService.Clear();
            tokenConfiguration = await _oidcClientService.Login(cancellationToken);
        }
        
        _tokenConfigurationService.Set(tokenConfiguration);

        return tokenConfiguration.AccessToken;
    }
}