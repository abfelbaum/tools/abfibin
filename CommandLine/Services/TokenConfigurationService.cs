using System.Text.Json;
using AbfiBin.CommandLine.Interfaces;
using AbfiBin.CommandLine.Models;

namespace AbfiBin.CommandLine.Services;

public class TokenConfigurationService : ITokenConfigurationService
{
    private readonly ISecureStorageService _secureStorage;

    public TokenConfigurationService(ISecureStorageService secureStorage)
    {
        _secureStorage = secureStorage;
    }

    public void Set(TokenConfiguration tokenConfiguration)
    {
        byte[] bytes = JsonSerializer.SerializeToUtf8Bytes(tokenConfiguration);

        _secureStorage.Verify();

        _secureStorage.Write(bytes);
    }

    public TokenConfiguration? Get()
    {
        _secureStorage.Verify();

        byte[] bytes = _secureStorage.Read();

        if (bytes.Length == 0)
        {
            return null;
        }
        
        try
        {
            return JsonSerializer.Deserialize<TokenConfiguration>(bytes);
        }
        catch (JsonException)
        {
            return null;
        }
    }

    public void Clear()
    {
        _secureStorage.Clear();
    }
}