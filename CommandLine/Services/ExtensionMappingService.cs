using AbfiBin.CommandLine.Interfaces;

namespace AbfiBin.CommandLine.Services;

public class ExtensionMappingService : IExtensionMappingService
{
    private readonly List<string> _highlighters = new()
    {
        "abap",
        "abc",
        "actionscript",
        "ada",
        "alda",
        "apache_conf",
        "apex",
        "applescript",
        "aql",
        "asciidoc",
        "asl",
        "assembly_x86",
        "autohotkey",
        "batchfile",
        "c9search",
        "c_cpp",
        "cirru",
        "clojure",
        "cobol",
        "coffee",
        "coldfusion",
        "crystal",
        "csharp",
        "csound_document",
        "csound_orchestra",
        "csound_score",
        "csp",
        "css",
        "curly",
        "d",
        "dart",
        "diff",
        "django",
        "dockerfile",
        "dot",
        "drools",
        "edifact",
        "eiffel",
        "ejs",
        "elixir",
        "elm",
        "erlang",
        "forth",
        "fortran",
        "fsharp",
        "fsl",
        "ftl",
        "gcode",
        "gherkin",
        "gitignore",
        "glsl",
        "gobstones",
        "golang",
        "graphqlschema",
        "groovy",
        "haml",
        "handlebars",
        "haskell",
        "haskell_cabal",
        "haxe",
        "hjson",
        "html",
        "html_elixir",
        "html_ruby",
        "ini",
        "io",
        "ion",
        "jack",
        "jade",
        "java",
        "javascript",
        "json",
        "json5",
        "jsoniq",
        "jsp",
        "jssm",
        "jsx",
        "julia",
        "kotlin",
        "latex",
        "latte",
        "less",
        "liquid",
        "lisp",
        "livescript",
        "logiql",
        "logtalk",
        "lsl",
        "lua",
        "luapage",
        "lucene",
        "makefile",
        "markdown",
        "mask",
        "matlab",
        "maze",
        "mediawiki",
        "mel",
        "mips",
        "mixal",
        "mushcode",
        "mysql",
        "nginx",
        "nim",
        "nix",
        "nsis",
        "nunjucks",
        "objectivec",
        "ocaml",
        "partiql",
        "pascal",
        "perl",
        "pgsql",
        "php",
        "php_laravel_blade",
        "pig",
        "plain_text",
        "powershell",
        "praat",
        "prisma",
        "prolog",
        "properties",
        "protobuf",
        "puppet",
        "python",
        "qml",
        "r",
        "raku",
        "razor",
        "rdoc",
        "red",
        "redshift",
        "rhtml",
        "robot",
        "rst",
        "ruby",
        "rust",
        "sac",
        "sass",
        "scad",
        "scala",
        "scheme",
        "scrypt",
        "scss",
        "sh",
        "sjs",
        "slim",
        "smarty",
        "smithy",
        "snippets",
        "soy_template",
        "space",
        "sparql",
        "sql",
        "sqlserver",
        "stylus",
        "svg",
        "swift",
        "tcl",
        "terraform",
        "tex",
        "text",
        "textile",
        "toml",
        "tsx",
        "turtle",
        "twig",
        "typescript",
        "vala",
        "vbscript",
        "velocity",
        "verilog",
        "vhdl",
        "visualforce",
        "wollok",
        "xml",
        "xquery",
        "yaml",
        "zeek"
    };

    private readonly Dictionary<string, IEnumerable<string>> _highlighterMappings = new()
    {
        { "abap", new[] { "sap-abap" } },
        { "actionscript", new[] { "as" } },
        { "apache_conf", new[] { "apache", "apacheconf" } },
        { "applescript", new[] { "osascript" } },
        { "asciidoc", new[] { "adoc" } },
        { "assembly_x86", new[] { "x86asm" } },
        { "batchfile", new[] { "dos", "bat", "cmd" } },
        { "c_cpp", new[] { "c", "h", "cpp", "hpp", "cc", "hh", "c++", "h++", "cxx", "hxx" } },
        { "clojure", new[] { "clj" } },
        { "cobol", new[] { "standard-cobol" } },
        { "coffee", new[] { "coffeescript", "cson", "iced" } },
        { "crystal", new[] { "cr" } },
        { "csharp", new[] { "cs" } },
        { "diff", new[] { "patch" } },
        { "django", new[] { "jinja" } },
        { "dockerfile", new[] { "docker" } },
        { "erlang", new[] { "erl" } },
        { "fortran", new[] { "f90", "f95" } },
        { "fsharp", new[] { "fs" } },
        { "gcode", new[] { "nc" } },
        { "golang", new[] { "go" } },
        { "graphqlschema", new[] { "graphql" } },
        { "handlebars", new[] { "hbs", "html.hbs", "html.handlebars" } },
        { "haskell", new[] { "hs" } },
        { "haxe", new[] { "hx" } },
        { "javascript", new[] { "js" } },
        { "julia", new[] { "julia-repl" } },
        { "kotlin", new[] { "kt" } },
        { "livescript", new[] { "ls" } },
        { "makefile", new[] { "mk", "mak", "make" } },
        { "markdown", new[] { "md", "mkdown", "mkd" } },
        { "nginx", new[] { "nginxconf" } },
        { "nim", new[] { "nimrod" } },
        { "objectivec", new[] { "mm", "objc", "obj-c", "obj-c++", "objective-c++" } },
        { "ocaml", new[] { "ml" } },
        { "pascal", new[] { "dpr", "dfm", "pas" } },
        { "perl", new[] { "pl", "pm" } },
        { "pgsql", new[] { "postgres", "postgresql" } },
        { "php_laravel_blade", new[] { "blade" } },
        { "plain_text", new[] { "txt" } },
        { "powershell", new[] { "ps", "ps1" } },
        { "puppet", new[] { "pp" } },
        { "python", new[] { "py", "gyp" } },
        { "razor", new[] { "cshtml", "razor-cshtml" } },
        { "red", new[] { "redbol", "rebol", "red-system" } },
        { "robot", new[] { "rf" } },
        { "ruby", new[] { "rb", "gemspec", "podspec", "thor", "irb" } },
        { "rust", new[] { "rs" } },
        { "scad", new[] { "openscad" } },
        { "sh", new[] { "bash", "zsh", "shell", "console" } },
        { "stylus", new[] { "styl" } },
        { "tcl", new[] { "tk" } },
        { "terraform", new[] { "tf", "hcl" } },
        { "twig", new[] { "craftcms" } },
        { "typescript", new[] { "ts" } },
        { "vbscript", new[] { "vbs" } },
        { "verilog", new[] { "v" } },
        { "xml", new[] { "xhtml", "rss", "atom", "xjb", "xsd", "xsl", "plist" } },
        { "xquery", new[] { "xpath", "xq" } },
        { "yaml", new[] { "yml" } }
    };

    public string? Map(string extension)
    {
        extension = extension.ToLowerInvariant();

        if (_highlighters.Contains(extension))
        {
            return extension;
        }

        KeyValuePair<string, IEnumerable<string>>? mapping = _highlighterMappings.FirstOrDefault(x => x.Value.Contains(extension));

        return mapping?.Key;
    }
}