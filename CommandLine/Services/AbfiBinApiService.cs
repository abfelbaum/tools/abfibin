using System.Net.Http.Json;
using AbfiBin.CommandLine.Interfaces;
using AbfiBin.CommandLine.Models;
using AbfiBin.Shared;
using FluentValidation;
using FluentValidation.Results;
using IdentityModel.Client;

namespace AbfiBin.CommandLine.Services;

public class AbfiBinApiService : IAbfiBinApiService
{
    private readonly HttpClient _httpClient;
    private readonly ITokenService _tokenService;
    private readonly IValidator<Bin> _binValidator;
    private readonly AppConfiguration _configuration;

    public AbfiBinApiService(HttpClient httpClient, ITokenService tokenService, IValidator<Bin> binValidator, AppConfiguration configuration)
    {
        _httpClient = httpClient;
        _tokenService = tokenService;
        _binValidator = binValidator;
        _configuration = configuration;
    }

    private async Task InitHttpClient(CancellationToken cancellationToken = default)
    {
        string token = await _tokenService.Get(cancellationToken);

        _httpClient.SetBearerToken(token);
        _httpClient.BaseAddress = _configuration.Address;
    }

    public async Task<Guid?> Create(Bin bin, CancellationToken cancellationToken = default)
    {
        ValidationResult result = await _binValidator.ValidateAsync(bin, cancellationToken);

        if (!result.IsValid)
        {
            Console.WriteLine("Some validation errors occured:");
            foreach (ValidationFailure failure in result.Errors)
            {
                Console.WriteLine($"{failure.PropertyName}: {failure.ErrorMessage}");
            }

            return null;
        }

        await InitHttpClient(cancellationToken);
        HttpResponseMessage response = await _httpClient.PostAsJsonAsync("api/v1/Bin", bin, cancellationToken: cancellationToken);

        if (!response.IsSuccessStatusCode || response.Headers.Location is null)
        {
            Console.WriteLine($"An error occured: {response.ReasonPhrase}");
            return null;
        }

        return await response.Content.ReadFromJsonAsync<Guid>(cancellationToken: cancellationToken);
    }

    public async Task<int> Delete(Guid id, CancellationToken cancellationToken = default)
    {
        await InitHttpClient(cancellationToken);
        HttpResponseMessage response = await _httpClient.DeleteAsync($"api/v1/Bin/{id:D}", cancellationToken);
        
        
        if (!response.IsSuccessStatusCode)
        {
            Console.WriteLine($"An error occured: {response.ReasonPhrase}");
            return 1;
        }

        return 0;
    }

    public async Task<Bin?> Read(Guid id, CancellationToken cancellationToken = default)
    {
        await InitHttpClient(cancellationToken);
        HttpResponseMessage response = await _httpClient.GetAsync($"api/v1/Bin/{id:D}", cancellationToken: cancellationToken);

        if (!response.IsSuccessStatusCode)
        {
            Console.WriteLine($"An error occured: {response.ReasonPhrase}");
            return null;
        }

        Bin? bin = await response.Content.ReadFromJsonAsync<Bin>(cancellationToken: cancellationToken);

        if (bin is null)
        {
            Console.WriteLine("An unknown error occured");
            return null;
        }

        return bin;
    }
}