﻿// See https://aka.ms/new-console-template for more information

// ReSharper disable once CheckNamespace

using System.CommandLine.Builder;
using System.CommandLine.Hosting;
using System.CommandLine.Parsing;
using AbfiBin.CommandLine.Commands;
using AbfiBin.CommandLine.Handlers;
using AbfiBin.CommandLine.Interfaces;
using AbfiBin.CommandLine.Middlewares;
using AbfiBin.CommandLine.Models;
using AbfiBin.CommandLine.Services;
using AbfiBin.Shared.Validators;
using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace AbfiBin.CommandLine;

class Program
{
    private static async Task<int> Main(string[] args)
    {
        var root = new AbfiBinCommand();

        root.AddCommand(new CreateCommand());
        root.AddCommand(new ReadCommand());
        root.AddCommand(new DeleteCommand());
        root.AddCommand(new ResetCommand());

        Parser runner = new CommandLineBuilder(root)
            .UseHost(_ => Host.CreateDefaultBuilder(args), builder =>
                builder.UseDefaultServiceProvider(options => { options.ValidateScopes = true; })
                    .ConfigureServices((_, services) =>
                    {
                        services.AddSingleton<ISecureStorageService, SecureStorageService>();
                        services.AddSingleton<IOidcClientService, OidcClientService>();
                        services.AddSingleton<AppConfiguration>();

                        services.AddTransient<ITokenConfigurationService, TokenConfigurationService>();
                        services.AddTransient<ITokenService, TokenService>();
                        services.AddTransient<IExtensionMappingService, ExtensionMappingService>();

                        services.AddValidatorsFromAssemblyContaining<BinValidator>(ServiceLifetime.Transient);
                        services.AddHttpClient<IAbfiBinApiService, AbfiBinApiService>();
                    })
                    .ConfigureLogging(loggingBuilder => { loggingBuilder.SetMinimumLevel(LogLevel.Warning); })
                    .UseCommandHandler<CreateCommand, CreateCommandHandler>()
                    .UseCommandHandler<ResetCommand, ResetCommandHandler>()
                    .UseCommandHandler<DeleteCommand, DeleteCommandHandler>()
                    .UseCommandHandler<ReadCommand, ReadCommandHandler>())
            .AddMiddleware(ConfigurationMiddleware.GlobalConfigurationMiddleware)
            .UseDefaults()
            .Build();
        return await runner.InvokeAsync(args);
    }
}