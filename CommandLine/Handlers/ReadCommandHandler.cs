using System.CommandLine.Invocation;
using System.Text;
using AbfiBin.CommandLine.Interfaces;
using AbfiBin.Shared;

namespace AbfiBin.CommandLine.Handlers;

public class ReadCommandHandler : ICommandHandler
{
    private readonly IAbfiBinApiService _apiService;
    public Guid Id { get; set; } = default;

    public DirectoryInfo? Output { get; set; } = null;

    public ReadCommandHandler(IAbfiBinApiService apiService)
    {
        _apiService = apiService;
    }

    public int Invoke(InvocationContext context)
    {
        throw new NotImplementedException();
    }

    public async Task<int> InvokeAsync(InvocationContext context)
    {
        Bin? bin = await _apiService.Read(Id, context.GetCancellationToken());

        if (bin is null)
        {
            return 1;
        }

        DirectoryInfo output = Output ?? new DirectoryInfo(Directory.GetCurrentDirectory());

        if (!output.Exists)
        {
            output.Create();
        }

        foreach ((BinFile file, int i) in bin.Files.Select((x, i) => (x, i)))
        {
            string fileName = file.Name ?? $"File {i+1}.txt";

            await File.WriteAllTextAsync(Path.Combine(output.FullName, fileName), file.Content, Encoding.Default,
                context.GetCancellationToken());
        }

        return 0;
    }
}