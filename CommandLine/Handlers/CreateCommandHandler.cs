using System.CommandLine.Invocation;
using System.Text;
using AbfiBin.CommandLine.Interfaces;
using AbfiBin.CommandLine.Models;
using AbfiBin.Shared;

namespace AbfiBin.CommandLine.Handlers;

public class CreateCommandHandler : ICommandHandler
{
    private readonly IAbfiBinApiService _apiService;
    private readonly AppConfiguration _configuration;
    private readonly IExtensionMappingService _mappingService;

    public CreateCommandHandler(IAbfiBinApiService apiService, AppConfiguration configuration,
        IExtensionMappingService mappingService)
    {
        _apiService = apiService;
        _configuration = configuration;
        _mappingService = mappingService;
    }

    public IEnumerable<FileInfo>? Files { get; set; } = null;
    public string? Name { get; set; }

    private readonly Bin _bin = new()
    {
        Files = new List<BinFile>()
    };

    public int Invoke(InvocationContext context)
    {
        throw new NotImplementedException();
    }

    public async Task<int> InvokeAsync(InvocationContext context)
    {
        if (context.Console.IsInputRedirected)
        {
            ReadFromStdIn();
        }

        if (Files is not null)
        {
            int result = await ReadFromFiles(Files, context.GetCancellationToken());

            if (result != 0)
            {
                return result;
            }
        }

        _bin.Name = Name;

        Guid? guid = await _apiService.Create(_bin, context.GetCancellationToken());

        if (guid is null)
        {
            return 1;
        }

        Console.WriteLine($"View your bin at: {_configuration.Address}bin/{guid:D}");

        return 0;
    }

    private void ReadFromStdIn()
    {
        var builder = new StringBuilder();
        while (Console.ReadLine() is { } line)
        {
            builder.AppendLine(line);
        }
        
        _bin.Files.Add(new BinFile
        {
            Content = builder.ToString()
        });
    }

    private async Task<int> ReadFromFiles(IEnumerable<FileInfo> paths, CancellationToken cancellationToken = default)
    {
        var fileReaders = new List<(FileInfo File, Task<string> Task)>();

        foreach (FileInfo file in paths)
        {
            if (!file.Exists)
            {
                Console.WriteLine($"File does not exist: {file.FullName}");
                return 1;
            }
            
            fileReaders.Add((file, File.ReadAllTextAsync(file.FullName, cancellationToken)));
        }

        IEnumerable<Task<string>> tasks = fileReaders.Select(x => x.Task);
        await Task.WhenAll(tasks);

        foreach ((FileInfo file, Task<string> content) in fileReaders)
        {
            _bin.Files.Add(new BinFile
            {
                Name = file.Name,
                Highlighter = _mappingService.Map(file.Extension.TrimStart('.')),
                Content = await content
            });
        }

        return 0;
    }
}