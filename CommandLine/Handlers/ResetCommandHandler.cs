using System.CommandLine.Invocation;
using AbfiBin.CommandLine.Interfaces;

namespace AbfiBin.CommandLine.Handlers;

public class ResetCommandHandler : ICommandHandler
{
    private readonly ITokenConfigurationService _tokenConfiguration;

    public ResetCommandHandler(ITokenConfigurationService tokenConfiguration)
    {
        _tokenConfiguration = tokenConfiguration;
    }

    public int Invoke(InvocationContext context)
    {
        throw new NotImplementedException();
    }

    public Task<int> InvokeAsync(InvocationContext context)
    {
        _tokenConfiguration.Clear();
        Console.WriteLine("Cleared!");

        return Task.FromResult(0);
    }
}