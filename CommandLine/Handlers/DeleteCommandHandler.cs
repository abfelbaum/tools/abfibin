using System.CommandLine.Invocation;
using AbfiBin.CommandLine.Interfaces;

namespace AbfiBin.CommandLine.Handlers;

public class DeleteCommandHandler : ICommandHandler
{
    private readonly IAbfiBinApiService _apiService;
    public Guid Id { get; set; } = default;

    public DeleteCommandHandler(IAbfiBinApiService apiService)
    {
        _apiService = apiService;
    }

    public int Invoke(InvocationContext context)
    {
        throw new NotImplementedException();
    }

    public async Task<int> InvokeAsync(InvocationContext context)
    {
        int result = await _apiService.Delete(Id, context.GetCancellationToken());

        return result;
    }
}