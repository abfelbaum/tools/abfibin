# AbfiBin CommandLine

## Description

Command line utility for [AbfiBin](https://git.abfelbaum.dev/abfelbaum/tools/abfibin). Supports creating, downloading and deleting bins.

## Installation

A package is provided via NuGet:

```shell
dotnet tool install -g AbfiBin.CommandLine
abfibin --help
```

## Update

Updating via NuGet:

```shell
dotnet tool update -g AbfiBin.CommandLine
abfibin --help
```

## Uninstall

Uninstalling via NuGet:

```shell
dotnet tool uninstall -g AbfiBin.CommandLine
abfibin
```

## Support

You can create issues via GitLab ServiceDesk by sending an email to reply+abfelbaum-tools-abfibin-14-issue-@git.abfelbaum.dev.

These issues are not visible for other users by default.

## License

The command line utility is licensed under GPL v3

## Other

Please have a look at https://wiki.abfelbaum.dev/en/projects/general-information.