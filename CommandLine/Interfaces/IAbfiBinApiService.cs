using AbfiBin.Shared;

namespace AbfiBin.CommandLine.Interfaces;

public interface IAbfiBinApiService
{
    public Task<Guid?> Create(Bin bin, CancellationToken cancellationToken = default);
    public Task<int> Delete(Guid id, CancellationToken cancellationToken = default);
    public Task<Bin?> Read(Guid id, CancellationToken cancellationToken = default);
}