namespace AbfiBin.CommandLine.Interfaces;

public interface ITokenService
{
    public Task<string> Get(CancellationToken cancellationToken = default);
}