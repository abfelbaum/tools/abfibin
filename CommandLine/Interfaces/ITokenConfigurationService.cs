using AbfiBin.CommandLine.Models;

namespace AbfiBin.CommandLine.Interfaces;

public interface ITokenConfigurationService
{
    public void Set(TokenConfiguration tokenConfiguration);
    public TokenConfiguration? Get();
    public void Clear();
}