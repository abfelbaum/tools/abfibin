namespace AbfiBin.CommandLine.Interfaces;

public interface ISecureStorageService
{
    public void Verify();
    public void Write(byte[] data);
    public byte[] Read();
    public void Clear();
}