using AbfiBin.CommandLine.Models;

namespace AbfiBin.CommandLine.Interfaces;

public interface IOidcClientService
{
    public Task<TokenConfiguration> Login(CancellationToken cancellationToken = default);
    public Task<bool> IsValid(TokenConfiguration tokenConfiguration, CancellationToken cancellationToken = default);
    public Task<TokenConfiguration> Refresh(TokenConfiguration tokenConfiguration, CancellationToken cancellationToken = default);
}