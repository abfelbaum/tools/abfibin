namespace AbfiBin.CommandLine.Interfaces;

public interface IExtensionMappingService
{
    public string? Map(string extension);
}