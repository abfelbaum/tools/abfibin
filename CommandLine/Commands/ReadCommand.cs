using System.CommandLine;

namespace AbfiBin.CommandLine.Commands;

public class ReadCommand : Command
{
    public ReadCommand() : base("read", "Download a bin")
    {
        AddArgument(new Argument<Guid>("id", "The id of the bin"));
        AddOption(new Option<DirectoryInfo>(new[] { "--output", "-o" }, "The path where the files should be put"));
    }
}