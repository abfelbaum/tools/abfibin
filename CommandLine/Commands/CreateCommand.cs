using System.CommandLine;

namespace AbfiBin.CommandLine.Commands;

public class CreateCommand : Command
{
    public CreateCommand() : base("create", "Create a bin, reads from stdin")
    {
        AddOption(new Option<IEnumerable<FileInfo>>(new []{ "--files", "-f" }, "One or multiple paths to one or multiple files that should be uploaded in the bin")
        {
            AllowMultipleArgumentsPerToken = true
        });
        AddOption(new Option<string>(new []{ "--name", "-n"}, "Name of the bin"));
    }
}