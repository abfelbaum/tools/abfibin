using System.CommandLine;
using Microsoft.Identity.Client.Extensions.Msal;

namespace AbfiBin.CommandLine.Commands;

public static class GlobalOptions
{
    public static readonly Option<Uri> Address = new("--addr",
        () => new Uri(Environment.GetEnvironmentVariable("ABFIBIN_ADDR") ?? "https://bin.abfelbaum.dev"),
        "The address of the AbfiBin instance")
    {
        IsHidden = true,
        IsRequired = true
    };

    public static readonly Option<Uri> Authority = new("--authority",
        () => new Uri(Environment.GetEnvironmentVariable("ABFIBIN_AUTHORITY") ?? "https://auth.abfelbaum.dev"),
        "The oidc authority")
    {
        IsHidden = true,
        IsRequired = true
    };

    public static readonly Option<string> ClientId = new("--clientId",
        () => Environment.GetEnvironmentVariable("ABFIBIN_CLIENTID") ?? "184115770781941853@abfibin",
        "The oidc client id")
    {
        IsHidden = true,
        IsRequired = true
    };

    public static readonly Option<Uri> RedirectUri = new("--redirect-uri",
        () => new Uri(Environment.GetEnvironmentVariable("ABFIBIN_REDIRECT_URI") ?? "http://127.0.0.1:10587"),
        "The oidc redirect uri")
    {
        IsHidden = true,
        IsRequired = true
    };

    public static readonly Option<int> RedirectPort = new("--redirect-port",
        () => int.TryParse(Environment.GetEnvironmentVariable("ABFIBIN_REDIRECT_PORT"), out int port) ? port : 10587,
        "The port the browser should listen on")
    {
        IsHidden = true,
        IsRequired = true
    };

    public static readonly Option<string> Scope = new("--scope",
        () => Environment.GetEnvironmentVariable("ABFIBIN_SCOPE") ?? "openid profile offline_access",
        "The openid scope")
    {
        IsHidden = true,
        IsRequired = true
    };

    public static readonly Option<string> CacheFileName = new("--cache-file-name",
        () => Environment.GetEnvironmentVariable("ABFIBIN_CACHE_FILE_NAME") ?? "abfibin.cache.txt",
        "The cache file when using unencrypted token storage")
    {
        IsHidden = true,
        IsRequired = true
    };

    public static readonly Option<string> CacheDirectory = new("--cache-directory",
        () => Environment.GetEnvironmentVariable("ABFIBIN_CACHE_DIRECTORY") ??
              $"{MsalCacheHelper.UserRootDirectory}/.config/abfibin",
        "The cache directory when using unencrypted token storage")
    {
        IsHidden = true,
        IsRequired = true
    };

    public static readonly Option<string?> Token = new("--token",
        () => Environment.GetEnvironmentVariable("ABFIBIN_TOKEN"),
        "An access token to override token retrieval"
    )
    {
        IsHidden = true,
        IsRequired = false
    };
}