using System.CommandLine;

namespace AbfiBin.CommandLine.Commands;

public class AbfiBinCommand : RootCommand
{
    public AbfiBinCommand() : base("AbfiBin CommandLine Utility")
    {
        AddGlobalOption(GlobalOptions.Address);
        AddGlobalOption(GlobalOptions.Authority);
        AddGlobalOption(GlobalOptions.CacheDirectory);
        AddGlobalOption(GlobalOptions.CacheFileName);
        AddGlobalOption(GlobalOptions.Scope);
        AddGlobalOption(GlobalOptions.ClientId);
        AddGlobalOption(GlobalOptions.RedirectPort);
        AddGlobalOption(GlobalOptions.RedirectUri);
        AddGlobalOption(GlobalOptions.Token);
    }
}