using System.CommandLine;

namespace AbfiBin.CommandLine.Commands;

public class ResetCommand : Command
{
    public ResetCommand() : base("reset", "Removes any saved data from the system")
    {
    }
}