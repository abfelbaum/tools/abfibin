using System.CommandLine;

namespace AbfiBin.CommandLine.Commands;

public class DeleteCommand : Command
{
    public DeleteCommand() : base("delete", "Deletes a bin")
    {
        AddArgument(new Argument<Guid>("id", "Id of the bin to delete"));
    }
}