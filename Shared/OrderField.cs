namespace AbfiBin.Shared;

public enum OrderField
{
    Created,
    Name,
    Owner
}