namespace AbfiBin.Shared;

public class ClientAuthentication
{
    public string Authority { get; set; } = null!;
    public string ClientId { get; set; } = null!;
    public string ResponseMode { get; set; } = "query";
    public string Scope { get; set; } = "openid profile offline_access";
}