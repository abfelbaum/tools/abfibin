using FluentValidation;
using Microsoft.Extensions.Options;

namespace AbfiBin.Shared.Validators;

public class BinValidator : AbstractValidator<Bin>
{
    public BinValidator(IOptions<ClientOptions> options)
    {
        RuleFor(x => x.Name).MaximumLength(100);
        RuleFor(x => x.Files).NotNull().NotEmpty().Must(x => x.Count <= 10);
        RuleForEach(x => x.Files).NotNull().SetValidator(new BinFileValidator(options));
    }
}