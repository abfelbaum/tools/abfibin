using FluentValidation;
using Microsoft.Extensions.Options;

namespace AbfiBin.Shared.Validators;

public class BinFileValidator : AbstractValidator<BinFile>
{
    public BinFileValidator(IOptions<ClientOptions> options)
    {
        RuleFor(x => x.Name).MaximumLength(100);
        RuleFor(x => x.Content).NotNull().NotEmpty().MaximumLength(400000);
        RuleFor(x => x.Highlighter).Custom((highlighter, context) =>
        {
            if (highlighter is null)
            {
                return;
            }

            if (options.Value?.Highlighters is null || options.Value.Highlighters.Contains(highlighter, StringComparer.InvariantCultureIgnoreCase))
            {
                return;
            }

            context.AddFailure(nameof(BinFile.Highlighter), "Invalid highlighter");
        });
    }
}