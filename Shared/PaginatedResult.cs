using System.ComponentModel.DataAnnotations;

namespace AbfiBin.Shared;

public class PaginatedResult<T>
{
    [Required] public int Total { get; set; }

    [Required] public IAsyncEnumerable<T> Items { get; set; } = null!;
}