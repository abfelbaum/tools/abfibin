using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace AbfiBin.Shared;

public class Bin
{
    [Key] public Guid Id { get; set; } = Guid.NewGuid();
    public virtual List<BinFile> Files { get; set; } = null!;
    public string? Owner { get; set; }
    public DateTime? Created { get; set; }
    public string? Name { get; set; }
}

public class BinFile
{
    [Key] public Guid Id { get; set; } = Guid.NewGuid();
    public string? Name { get; set; }
    public string Content { get; set; } = "";

    public string? Highlighter { get; set; }
    [JsonIgnore] public int? Index { get; set; }

    // ReSharper disable once UnusedAutoPropertyAccessor.Global
    [JsonIgnore] public virtual Bin? Bin { get; set; }
}