namespace AbfiBin.Shared;

public class ClientMappings
{
    public Dictionary<string, IEnumerable<string>> Highlighters { get; set; } = null!;
}