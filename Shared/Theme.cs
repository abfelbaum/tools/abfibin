namespace AbfiBin.Shared;

public class Theme
{
    public string Name { get; set; } = null!;
    public bool IsDark { get; set; }
}