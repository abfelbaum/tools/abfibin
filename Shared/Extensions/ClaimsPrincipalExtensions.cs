using System.Security.Claims;
using System.Text.Json;

namespace AbfiBin.Shared.Extensions;

public static class ClaimsPrincipalExtensions
{
    private const string RoleClaim = "urn:zitadel:iam:org:project:roles";

    public static bool IsInAnyZitadelRole(this ClaimsPrincipal user, params string[] roles)
    {
        Dictionary<string, Dictionary<string, string>>? decoded = DecodeRoles(user);
        return decoded is not null && roles.Any(decoded.ContainsKey);
    }

    public static bool IsInAllZitadelRoles(this ClaimsPrincipal user, params string[] roles)
    {
        Dictionary<string, Dictionary<string, string>>? decoded = DecodeRoles(user);
        return decoded is not null && roles.All(decoded.ContainsKey);
    }

    private static Dictionary<string, Dictionary<string, string>>? DecodeRoles(ClaimsPrincipal user)
    {
        Claim? claim = user.FindFirst(x => x.Type == RoleClaim);

        return claim is null
            ? null
            : JsonSerializer.Deserialize<Dictionary<string, Dictionary<string, string>>>(claim.Value);
    }
}