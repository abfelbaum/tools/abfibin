namespace AbfiBin.Shared;

public class ClientDefaults
{
    public string Highlighter { get; set; } = null!;
    public string Keybindings { get; set; } = null!;
    public string Theme { get; set; } = null!;
}