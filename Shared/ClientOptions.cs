namespace AbfiBin.Shared;

public class ClientOptions
{
    public List<string> Highlighters { get; set; } = null!;
    public List<Theme> Themes { get; set; } = null!;
    public List<string> Keybindings { get; set; } = null!;
    public ClientMappings Mappings { get; set; } = null!;

    public ClientDefaults Defaults { get; set; } = null!;
    public ClientAuthentication Authentication { get; set; } = null!;
}