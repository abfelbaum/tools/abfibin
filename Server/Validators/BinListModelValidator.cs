using AbfiBin.Server.Models;
using FluentValidation;

namespace AbfiBin.Server.Validators;

public class BinListModelValidator : AbstractValidator<BinListModel>
{
    public BinListModelValidator()
    {
        RuleFor(x => x.Id).NotEmpty().NotNull();
        RuleFor(x => x.Created).NotEmpty().NotNull();
        RuleFor(x => x.Name).MaximumLength(100);
        RuleFor(x => x.Owner).NotEmpty().NotNull();
    }
}