using AbfiBin.Server.Data;
using Microsoft.EntityFrameworkCore;

namespace AbfiBin.Server.Services;

public class DbMigrationHostedService : IHostedService
{
    private readonly IServiceScopeFactory _scopeFactory;

    public DbMigrationHostedService(IServiceScopeFactory scopeFactory)
    {
        _scopeFactory = scopeFactory;
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        await using AsyncServiceScope scope = _scopeFactory.CreateAsyncScope();

        BinDbContext context = scope.ServiceProvider.GetRequiredService<BinDbContext>();

        if ((await context.Database.GetPendingMigrationsAsync(cancellationToken)).Any())
        {
            await context.Database.MigrateAsync(cancellationToken);
        }
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }
}