namespace AbfiBin.Server.Models;

public class BinListModel
{
    public Guid Id { get; init; } = Guid.NewGuid();
    public string Owner { get; init; } = null!;
    public DateTime Created { get; init; }
    public string? Name { get; init; }
}