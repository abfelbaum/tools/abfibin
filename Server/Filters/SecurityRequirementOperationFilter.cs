using Microsoft.AspNetCore.Authorization;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace AbfiBin.Server.Filters;

public class SecurityRequirementsOperationFilter : IOperationFilter
{
    private readonly OpenApiMediaType _openApiMediaType = new()
    {
        Schema = new OpenApiSchema
        {
            Reference = new OpenApiReference
            {
                Type = ReferenceType.Schema,
                Id = "ProblemDetails"
            }
        }
    };

    private IDictionary<string, OpenApiMediaType> Content => new Dictionary<string, OpenApiMediaType>
    {
        { "text/plain", _openApiMediaType },
        { "application/json", _openApiMediaType },
        { "text/json", _openApiMediaType }
    };

    public void Apply(OpenApiOperation operation, OperationFilterContext context)
    {
        // Policy names map to scopes
        IEnumerable<AuthorizeAttribute> attribute = context.MethodInfo
            .GetCustomAttributes(true)
            .OfType<AuthorizeAttribute>()
            .Distinct();

        if (!attribute.Any())
        {
            return;
        }

        if (!operation.Responses.ContainsKey(StatusCodes.Status401Unauthorized.ToString()))
        {
            operation.Responses.Add("401", new OpenApiResponse { Description = "Unauthorized", Content = Content });
        }

        if (!operation.Responses.ContainsKey(StatusCodes.Status403Forbidden.ToString()))
        {
            operation.Responses.Add("403", new OpenApiResponse { Description = "Forbidden", Content = Content });
        }

        operation.Security = new List<OpenApiSecurityRequirement>
        {
            new()
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Id = "bearer",
                            Type = ReferenceType.SecurityScheme
                        }
                    },
                    new List<string>()
                }
            }
        };
    }
}