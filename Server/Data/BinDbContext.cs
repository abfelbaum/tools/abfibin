using AbfiBin.Shared;
using Microsoft.EntityFrameworkCore;

namespace AbfiBin.Server.Data;

public class BinDbContext : DbContext
{
    public BinDbContext(DbContextOptions<BinDbContext> options) : base(options)
    {
    }

    public DbSet<Bin> Bins { get; set; } = null!;
    public DbSet<BinFile> Files { get; set; } = null!;

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Bin>()
            .HasMany(x => x.Files)
            .WithOne(x => x.Bin).OnDelete(DeleteBehavior.Cascade);

        base.OnModelCreating(modelBuilder);
    }
}