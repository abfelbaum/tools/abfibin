using System.Security.Claims;
using AbfiBin.Server.Data;
using AbfiBin.Server.Models;
using AbfiBin.Shared;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MudBlazor;
using Zitadel.Authentication;

namespace AbfiBin.Server.Controllers;

[ApiController]
[Route("api/v1/[controller]")]
public class BinController : ControllerBase
{
    private readonly IValidator<Bin> _binValidator;
    private readonly BinDbContext _context;

    public BinController(IValidator<Bin> binValidator, BinDbContext context)
    {
        _binValidator = binValidator;
        _context = context;
    }

    /// <summary>
    ///     Creates a new bin
    /// </summary>
    /// <remarks>Requires login</remarks>
    /// <param name="bin">The bin data</param>
    /// <returns></returns>
    [HttpPost]
    [Authorize(AuthenticationSchemes = "ZITADEL")]
    [ProducesResponseType(typeof(Guid), StatusCodes.Status201Created)]
    [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> Create([FromBody] Bin bin)
    {
        ValidationResult validationResult = await _binValidator.ValidateAsync(bin);

        if (!validationResult.IsValid)
        {
            return BadRequest(new ValidationProblemDetails(validationResult.ToDictionary()));
        }

        bin.Id = Guid.NewGuid();
        bin.Created = DateTime.Now.ToUniversalTime();
        bin.Owner = User.FindFirstValue(OidcClaimTypes.Subject);

        foreach ((BinFile file, int i) in bin.Files.Select((x, i) => (x, i)))
        {
            file.Id = Guid.NewGuid();
            file.Index = i;
        }

        await _context.Bins.AddAsync(bin);

        await _context.SaveChangesAsync();

        return Created($"/api/v1/bin/{bin.Id}", bin.Id);
    }

    /// <summary>
    ///     Gets a bin with all its data
    /// </summary>
    /// <param name="id" example="d91a3a8a-cc32-4eed-b72b-865c345e4fb9">Bin id</param>
    /// <returns>The bin</returns>
    [HttpGet("{id:guid}")]
    [ProducesResponseType(typeof(Bin), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Get(Guid id)
    {
        Bin? bin = await _context.Bins.Include(x => x.Files).AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);

        if (bin is null)
        {
            return NotFound();
        }

        bin.Files = bin.Files.OrderBy(x => x.Index).ToList();
        bin.Owner = null;

        return Ok(bin);
    }

    /// <summary>
    ///     Gets the list of all bins created by the current user
    /// </summary>
    /// <remarks>Requires login</remarks>
    /// <param name="skip" example="0">Skip for pagination</param>
    /// <param name="take" example="10">Take for pagination</param>
    /// <param name="search" example="HelloWorld">Search bin names, owners and ids</param>
    /// <param name="orderBy" example="Created">Order by field</param>
    /// <param name="orderDirection" example="Descending">Order direction</param>
    /// <returns>Paginated list of bins</returns>
    [HttpGet]
    [Authorize(AuthenticationSchemes = "ZITADEL")]
    [ProducesResponseType(typeof(PaginatedResult<BinListModel>), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetList(int skip = 0, int take = 10, string? search = null,
        OrderField orderBy = OrderField.Created,
        SortDirection orderDirection = SortDirection.Descending)
    {
        string userId = User.FindFirstValue(OidcClaimTypes.Subject);

        IQueryable<Bin> totalBins = _context.Bins.Where(x => x.Owner == userId);

        int total = await totalBins.CountAsync();

        IQueryable<Bin> bins = totalBins.Where(x =>
            (search == null) ||
            // ReSharper disable once ArrangeRedundantParentheses
            (
                ((x.Name != null) &&
                 (
                     x.Name.StartsWith(search) ||
                     x.Name.Contains(search)
                 )) ||
                (x.Id.ToString() == search)
            )
        );

        bins = orderBy switch
        {
            OrderField.Name => bins.OrderByDirection(orderDirection, x => x.Name),
            OrderField.Owner => bins.OrderByDirection(orderDirection, x => x.Owner),
            _ => bins.OrderByDirection(orderDirection, x => x.Created)
        };

        var result = new PaginatedResult<BinListModel>
        {
            Total = total,
            Items = bins.Skip(skip).Take(take).AsNoTracking().AsAsyncEnumerable().Select(x => new BinListModel
                {
                    Id = x.Id,
                    Created = x.Created!.Value,
                    Name = x.Name,
                    Owner = x.Owner!
                }
            )
        };

        return Ok(result);
    }

    /// <summary>
    ///     Deletes a bin
    /// </summary>
    /// <remarks>Requires login</remarks>
    /// <param name="id" example="d91a3a8a-cc32-4eed-b72b-865c345e4fb9">Bin id</param>
    /// <returns>Nothing!</returns>
    /// <response code="204">Bin deleted</response>
    /// <response code="404">Bin not found or not the owner of the bin</response>
    [HttpDelete("{id:guid}")]
    [Authorize(AuthenticationSchemes = "ZITADEL")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Delete(Guid id)
    {
        string userId = User.FindFirstValue(OidcClaimTypes.Subject);
        Bin? bin = await _context.Bins.FirstOrDefaultAsync(x => (x.Id == id) && (x.Owner == userId));

        if (bin is null)
        {
            return Forbid();
        }

        _context.Bins.Remove(bin);
        await _context.SaveChangesAsync();

        return NoContent();
    }
}