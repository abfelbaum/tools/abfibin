using AbfiBin.Server.Data;
using AbfiBin.Server.Models;
using AbfiBin.Shared;
using AbfiBin.Shared.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MudBlazor;

namespace AbfiBin.Server.Controllers;

[ApiController]
[Route("api/v1/[controller]")]
public class AdminController : ControllerBase
{
    private readonly BinDbContext _context;

    public AdminController(BinDbContext context)
    {
        _context = context;
    }

    /// <summary>
    ///     Gets the list of all bins created on the instance
    /// </summary>
    /// <remarks>Requires admin role</remarks>
    /// <param name="skip" example="0">Skip for pagination</param>
    /// <param name="take" example="10">Take for pagination</param>
    /// <param name="search" example="HelloWorld">Search bin names, owners and ids</param>
    /// <param name="orderBy" example="Created">Order by field</param>
    /// <param name="orderDirection" example="Descending">Order direction</param>
    /// <returns>Paginated list of bins</returns>
    [HttpGet]
    [Authorize(AuthenticationSchemes = "ZITADEL")]
    [ProducesResponseType(typeof(PaginatedResult<BinListModel>), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status403Forbidden)]
    public async Task<IActionResult> GetList(int skip = 0, int take = 10, string? search = null,
        OrderField orderBy = OrderField.Created,
        SortDirection orderDirection = SortDirection.Descending)
    {
        if (!User.IsInAllZitadelRoles("admin"))
        {
            return Forbid();
        }

        IQueryable<Bin> totalBins = _context.Bins;

        int total = await totalBins.CountAsync();

        IQueryable<Bin> bins = totalBins.Where(x =>
            (search == null) ||
            // ReSharper disable once ArrangeRedundantParentheses
            (
                ((x.Name != null) &&
                 (
                     x.Name.StartsWith(search) ||
                     x.Name.Contains(search)
                 )) ||
                ((x.Owner != null) &&
                 (
                     x.Owner.StartsWith(search) ||
                     x.Owner.Contains(search)
                 )) ||
                (x.Id.ToString() == search)
            )
        );

        bins = orderBy switch
        {
            OrderField.Name => bins.OrderByDirection(orderDirection, x => x.Name),
            OrderField.Owner => bins.OrderByDirection(orderDirection, x => x.Owner),
            _ => bins.OrderByDirection(orderDirection, x => x.Created)
        };

        var result = new PaginatedResult<BinListModel>
        {
            Total = total,
            Items = bins.Skip(skip).Take(take).AsNoTracking().AsAsyncEnumerable().Select(x => new BinListModel
            {
                Id = x.Id,
                Created = x.Created!.Value,
                Name = x.Name,
                Owner = x.Owner!
            })
        };

        return Ok(result);
    }

    /// <summary>
    ///     Deletes a bin
    /// </summary>
    /// <remarks>Requires admin role</remarks>
    /// <param name="id" example="d91a3a8a-cc32-4eed-b72b-865c345e4fb9">Bin id</param>
    /// <returns>Nothing!</returns>
    /// <response code="204">Bin deleted</response>
    /// <response code="403">admin role required</response>
    /// <response code="404">Bin not found</response>
    [HttpDelete("{id:guid}")]
    [Authorize(AuthenticationSchemes = "ZITADEL")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status403Forbidden)]
    [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status404NotFound)]
    public async Task<IActionResult> Delete(Guid id)
    {
        if (!User.IsInAllZitadelRoles("admin"))
        {
            return Forbid();
        }

        Bin? bin = await _context.Bins.FirstOrDefaultAsync(x => x.Id == id);

        if (bin is null)
        {
            return NotFound();
        }

        _context.Bins.Remove(bin);
        await _context.SaveChangesAsync();

        return NoContent();
    }
}