using AbfiBin.Shared;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace AbfiBin.Server.Controllers;

[ApiController]
[Route("api/v1/[controller]")]
public class ConfigurationController : ControllerBase
{
    private readonly ClientOptions _options;

    public ConfigurationController(IOptions<ClientOptions> options)
    {
        _options = options.Value;
    }

    /// <summary>
    ///     Gets the configuration for the client
    /// </summary>
    /// <returns>Current configuration</returns>
    [HttpGet]
    [ProducesResponseType(typeof(ClientOptions), StatusCodes.Status200OK)]
    public IActionResult Get()
    {
        return Ok(_options);
    }
}