﻿using System.Reflection;
using System.Text.Json.Serialization;
using AbfiBin.Server.Data;
using AbfiBin.Server.Filters;
using AbfiBin.Server.Services;
using AbfiBin.Shared;
using AbfiBin.Shared.Validators;
using FluentValidation;
using Hellang.Middleware.ProblemDetails;
using MicroElements.Swashbuckle.FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Hosting.StaticWebAssets;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using Zitadel.Credentials;
using Zitadel.Extensions;

WebApplicationBuilder builder = WebApplication.CreateBuilder(args);

builder.Configuration.AddEnvironmentVariables(prefix: "AbfiBin_");
StaticWebAssetsLoader.UseStaticWebAssets(builder.Environment, builder.Configuration);
builder.Services.Configure<ClientOptions>(builder.Configuration.GetSection("Client"));

// Add services to the container.

builder.Services.AddControllers().AddJsonOptions(options =>
{
    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
});

builder.Services.AddAuthorization();
builder.Services.AddAuthentication()
    .AddZitadelIntrospection("ZITADEL", options =>
    {
        options.Authority = builder.Configuration["Authentication:Authority"];
        options.JwtProfile = Application.LoadFromJsonFile(builder.Configuration["Authentication:JwtProfilePath"]);
    });

builder.Services.AddProblemDetails();
builder.Services.AddValidatorsFromAssemblyContaining<BinValidator>();
builder.Services.AddHostedService<DbMigrationHostedService>();
builder.Services.AddHttpContextAccessor();
builder.Services.AddSwaggerGen(options =>
{
    options.OperationFilter<SecurityRequirementsOperationFilter>();
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Version = "v1",
        Title = "AbfiBin API",
        Description = "An ASP.NET Core Web API for creating bins",
        License = new OpenApiLicense
        {
            Name = "GNU AFFERO GENERAL PUBLIC LICENSE v3",
            Url = new Uri("https://git.abfelbaum.dev/abfelbaum/abfibin/-/blob/main/license")
        }
    });

    options.AddSecurityDefinition("bearer", new OpenApiSecurityScheme
    {
        Type = SecuritySchemeType.Http,
        Scheme = "bearer",
        BearerFormat = "JWT",
        Description = "JWT Authorization header using the Bearer scheme."
    });

    options.EnableAnnotations();

    var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
});
builder.Services.AddFluentValidationRulesToSwagger();

builder.Services.AddDbContext<BinDbContext>(options =>
{
    options.UseNpgsql(builder.Configuration.GetConnectionString("Default"));
});

WebApplication app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseWebAssemblyDebugging();
}

app.UseProblemDetails();

app.UseBlazorFrameworkFiles();
app.UseStaticFiles();

app.UseRouting();

app.UseSwagger(options => { options.RouteTemplate = "api/{documentName}/docs/openapi.json"; });
app.UseReDoc(options =>
{
    options.RoutePrefix = "api/v1/docs";
    options.SpecUrl = "openapi.json";
    options.ExpandResponses("200,201");
    options.HideLoading();
});

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();
app.MapFallbackToFile("index.html");

await app.RunAsync();