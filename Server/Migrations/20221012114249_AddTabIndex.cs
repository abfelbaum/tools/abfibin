﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AbfiBin.Server.Migrations
{
    public partial class AddTabIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Index",
                table: "Files",
                type: "integer",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Index",
                table: "Files");
        }
    }
}
