# This should best be equal to project name, and also the binary name without extension. This defines the $${NOMAD_JOB_NAME}
job "abfibin-testing" {
  datacenters = ["luna"]
  type        = "service"

  update {
    stagger          = "60s"
    max_parallel     = 1
    min_healthy_time = "60s"
    healthy_deadline = "5m"
  }

  # Should probably be equal to the Job Name in most cases. This defines the $${NOMAD_GROUP_NAME}
  group "database" {
    count = 1

    network {
      mode = "bridge"
    }
    
    service {
      name = "abfibin-testing-pgsql"
      port = 5432

      connect {
        sidecar_service {}
      }
    }

    volume "data" {
      type            = "csi"
      source          = "abfibin-testing-pgsql"
      read_only       = false
      attachment_mode = "file-system"
      access_mode     = "multi-node-multi-writer"
    }

    # Should probably be equal to the Job/Group Name in most cases. This defines the $${NOMAD_TASK_NAME}
    task "postgres" {
      driver = "docker"

      config {
        image = "postgres:13.4-alpine"
      }

      volume_mount {
        volume      = "data"
        destination = "/var/lib/postgresql/data/"
      }

      vault {
        env      = false
        policies = ["abfibin-testing"]
      }

      template {
        destination = "$${NOMAD_SECRETS_DIR}/environment.database.env"
        env         = true
        data        = <<EOF
{{ with secret "kv/data/tools/abfibin-testing" }}
          POSTGRES_USER={{.Data.data.DB_USER}}
          POSTGRES_PASSWORD={{.Data.data.DB_PASSWORD}}
          POSTGRES_DB={{.Data.data.DB_DATABASE}}
{{ end }}
        EOF
      }
    }
  }

  group "abfibin" {
    count = 1

    network {
      mode = "bridge"
    }

    service {
      name = "abfibin-testing"
      port = 80

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "abfibin-testing-pgsql"
              local_bind_port  = 5432
            }
          }
        }
      }

      tags = [
        "traefik.enable=true",
        "traefik.http.routers.abfibin-testing.rule=Host(`testing.bin.abfelbaum.dev`)",
        "traefik.http.routers.abfibin-testing.entrypoints=websecure",
        "traefik.http.routers.abfibin-testing.tls.certresolver=leresolver",
      ]
    }

    task "abfibin" {
      driver = "docker"

      config {
        image = "${artifact.image}:${artifact.tag}"
      }

      vault {
        env      = false
        policies = ["abfibin-testing"]
      }

      template {
        destination = "$${NOMAD_SECRETS_DIR}/environment.env"
        env         = true
        data        = <<EOF
{{ with secret "kv/data/tools/abfibin-testing" }}
          AbfiBin_ConnectionStrings__Default="Host={{ env "NOMAD_UPSTREAM_IP_abfibin_testing_pgsql" }};Port={{ env "NOMAD_UPSTREAM_PORT_abfibin_testing_pgsql" }};Database={{.Data.data.DB_DATABASE}};Username={{.Data.data.DB_USER}};Password={{.Data.data.DB_PASSWORD}};"
          AbfiBin_Client__Authentication__ClientId="{{.Data.data.OIDC_CLIENT_ID}}"
{{ end }}
EOF
      }

      template {
        destination = "$${NOMAD_SECRETS_DIR}/jwtprofile.json"
        data        = <<EOF
{{ with secret "kv/data/tools/abfibin-testing" }}{{.Data.data.OIDC_SERVER_JSON}}{{ end }}
EOF
      }

      env {
        AbfiBin_Authentication__Authority         = "https://auth.abfelbaum.dev"
        AbfiBin_Authentication__JwtProfilePath    = "$${NOMAD_SECRETS_DIR}/jwtprofile.json"
        AbfiBin_Client__Authentication__Authority = "https://auth.abfelbaum.dev"
      }
    }
  }
}