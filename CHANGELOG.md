# Changelog

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.2.3](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/compare/1.2.2...1.2.3) (2022-12-26)


### Bug Fixes

* ci ([95db952](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/commit/95db952f575c3fe0da28a7731f9c707dea30db21))
* excluded paths ([397c1b2](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/commit/397c1b29cb4afaac9af4cef381e15b9b6dad0548))
* excluded paths in sast ([914d2ad](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/commit/914d2ad1e1476cbb45f8afa093e305e0fb59c994))
* old versions getting built despite invalid ci configuration ([faf8d00](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/commit/faf8d00c01ca4de7dfc9da76cf35506570b97fa6))
* semgrep needing too long ([2ad7e98](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/commit/2ad7e9880b7b27a207b9ba83ed86f9b1da4aedff))

## [1.2.2](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/compare/1.2.1...1.2.2) (2022-10-24)


### Bug Fixes

* **ci:** CHANGELOG.md contained beta versions ([5303d1d](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/commit/5303d1d7874d162e95ba88549daf60b09fb9876d))
* **ci:** Debugging ([0e52371](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/commit/0e5237198ec057008eccaa13374b8fff5f9607db))
* **ci:** is_container_update_old ([5cbab3a](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/commit/5cbab3ad85d3d9c71a06877170c406bb62417266))
* **ci:** Release patterns ([9744ed3](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/commit/9744ed328af739d78a5757a829d4f69f4d1b7571))
* **ci:** Release rules ([c993e33](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/commit/c993e33dd1953e920110e516249a39107b536a26))
* **ci:** Release script ([a372b35](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/commit/a372b350c0e00bcd566c8248d5b9c6e7631c4b56))
* **ci:** Remove releasecr ([c97d474](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/commit/c97d4746d050423bcdc10ff2726dce781d656f3a))
* **ci:** Rules for releasing ([53162f6](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/commit/53162f601526ef2f3c7a5a19b69b7547c4393634))
* debug ([7e82405](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/commit/7e824054703e6812885c30e0dfb547eb9181708a))

## [1.1.2](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/compare/1.1.1...1.1.2) (2022-10-23)


### Bug Fixes

* **ci:** Missing default values ([eb0770e](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/commit/eb0770ebef119c555bcc2bce6d5d5ef80087d3b7))

## [1.1.1](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/compare/1.1.0...1.1.1) (2022-10-23)


### Bug Fixes

* **ci:** Deployment tier for waypoint.hcl ([2898879](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/commit/2898879cf6521b5348fa4abb6e6748d6cadc73b3))

## [1.1.0](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/compare/1.0.0...1.1.0) (2022-10-23)


### Features

* **ci:** Better release management ([113b54b](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/commit/113b54bbea75e022aa3a5cf93cba1e86aa5c8834))


### Bug Fixes

* **ci:** Docker image name ([f376a65](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/commit/f376a659530fad3ec94a2f03c6a8e28722caf6b8))
* **ci:** Missing script tag ([7c150c8](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/commit/7c150c80f0819ff194bcc65aec3ba9dd8b9f4ebd))
* **ci:** Publish dependencies ([9c879b6](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/commit/9c879b65787be068c699c73224894ed6c6a451ae))

## 1.0.0 (2022-10-23)


### Features

* **ci:** Support for alpha releases ([c500b9f](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/commit/c500b9fbcadaab4c182eacad04de87c0a5df7077))
* Release 1.0 ([e1a9d9b](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/commit/e1a9d9bd3c3f4f672558173572e1f05310915fa6))
* Write out readmes ([c4c8c5a](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/commit/c4c8c5a907b46571d784dbe4b69d1c9b30ff8df5))


### Bug Fixes

* **ci:** Artifacts not passed ([32d6014](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/commit/32d6014081441a6b38fe5e3393c301577da453ff))
* **ci:** Dependency names ([6006007](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/commit/6006007e702f330be5bb6b763b8cd93bf31415fd))
* **ci:** NuGet push problem ([d11d238](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/commit/d11d2388ff1e1fc3933edbf5079c4273f81bf31a))
* **ci:** Release message ([4c980a8](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/commit/4c980a8876b578b98ec7b673b244d3f95e089687))
* **ci:** Release stage ([7be5614](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/commit/7be5614d946d667219964c18e35450c86e1845b6))
* **ci:** Replace gitlab release image with local one ([18fef0e](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/commit/18fef0e16cee98bd969447df1018a82a4df85c04))
* **ci:** Workspaces for publishing waypoint ([6b4784c](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/commit/6b4784c87d8beffb82d361deb1d0c30c001eac3f))
* Non-release builds ([540468a](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/commit/540468a6d668fd0ac188eb732750129602675918))
