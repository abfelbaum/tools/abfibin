FROM registry.git.abfelbaum.dev/abfelbaum/images/dotnet/aspnet:6.0
WORKDIR /app
COPY publish/ .
ENV ASPNETCORE_URLS="http://*:80"
ENV ASPNETCORE_ENVIRONMENT="Production"
ENTRYPOINT ["dotnet", "AbfiBin.Server.dll"]