# AbfiBin

## Description
An easy tool to put code in the web. Has an [CLI](https://git.abfelbaum.dev/abfelbaum/tools/abfibin/-/blob/main/CommandLine), supports multiple files per bin and requires OIDC authentication.

## Installation
Just use the nomad file :D

Otherwise:

1. Setup postgresql. I don't exactly know which versions are supported. I'm using 13.4.
2. Setup two ZITADEL applications, one for the api (JWT) and one for the webinterface (PKCE).
3. Set environment variables for configuration. Use the nomad files as a starting point for that.
4. Run!

## Support
You can create issues via GitLab ServiceDesk by sending an email to reply+abfelbaum-tools-abfibin-14-issue-@git.abfelbaum.dev.

These issues are not visible for other users by default.

## License
The Server and Client project are licensed under AGPL v3. The CommandLine project is licensed under GPL v3.

## Other

Please have a look at https://wiki.abfelbaum.dev/en/projects/general-information.